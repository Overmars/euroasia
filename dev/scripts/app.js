$(document).ready(function() {
    $('a[href="#"]').click(function(e){
        e.preventDefault();
    });

    $('.mobile-menu-trigger').on('click', function(){
        $(this).toggleClass('open');
        $('.m_mobile-menu').toggleClass('open');
        $('html, body').toggleClass('menu-opened');
        $('.m_mobile-menu li').removeClass('flip');
        if ( !$(this).hasClass('open') ) {
            $('.m_mobile-menu li').removeClass('flip');
        } else {
            $('.m_mobile-menu li').each(function(i){
                var row = $(this);
                 setTimeout(function() {
                   row.addClass('flip');
                 }, 150*i);
            });
        }
    });


    $('.scroll-form').click(function(){
        $("html, body").animate({ scrollTop: $(document).height()-$(window).height() - 150 }, 500);
        $('#form').addClass('animated');
    });

    $('#form-tab-2-trigger').on('click', function(){
        $(this).addClass('current');
        $('#form-tab-1-trigger').removeClass('current');
        $('.form-tab-1').fadeOut('250', function(){
            $('.form-tab-2').fadeIn('250');
        });
    });
    $('#form-tab-1-trigger').on('click', function(){
        $(this).addClass('current');
        $('#form-tab-2-trigger').removeClass('current');
        $('.form-tab-2').fadeOut('250', function(){
            $('.form-tab-1').fadeIn('250');
        });
    });
});